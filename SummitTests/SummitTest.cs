using SummitAlgo;

namespace SummitTests
{
    [TestClass]
    public class SummitTest
    {
        [TestMethod]
        public void TestA()
        {
            Assert.AreEqual(Solution.IsSummit("a"), true);
        }

        [TestMethod]
        public void TestStringFalse()
        {
            Assert.AreEqual(Solution.IsSummit("aba"),false);
        }

        [TestMethod]
        public void TestStringTrue()
        {
            Assert.AreEqual(Solution.IsSummit("abcdef"), true);
        }

        [TestMethod]
        public void TestSentence()
        {
            Assert.AreEqual(Solution.IsSummit("A'b c d e"), true);

        }

        [TestMethod]
        public void TestUpperCaseString() {

            Assert.AreEqual(Solution.IsSummit("ABCDE"), true);
        
        }

        [TestMethod]
        public void TestVoidString()
        {
            Assert.AreEqual(Solution.IsSummit(""), false);

        }
    }
}