﻿using System.Security.Cryptography.X509Certificates;

namespace SummitAlgo
{

    public class Solution
    {
        public static void Main(string[] args)
        {
            string mot = "";
            Console.WriteLine(IsSummit(mot));
        }
        public static bool IsSummit(string s)
        {
            bool result = true;

            if (s.Length == 0)
            {
                result = false;
            }
            else
            {
                string filtered = new(s.Where(char.IsLetterOrDigit).Select(char.ToLower).ToArray());
                char[] alpha = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z' };
                int indexCurr = 0;

                for (int i = 0; i < filtered.Length && result; i++)
                {
                    int alphaIndex = Array.IndexOf(alpha, filtered[i]);
                    if (indexCurr > alphaIndex)
                    {
                        result = false;
                    }
                    indexCurr = alphaIndex;


                }
            }

            return result;
        }
    }
}